package main

import (
	"gitlab.com/abdirahman04/todo/internal/app/handlers"
	"log"
	"net/http"
)

func main() {
	port := ":8989"
	mux := http.NewServeMux()

	mux.HandleFunc("GET /todo/", handlers.Get)
	mux.HandleFunc("POST /todo/", handlers.Post)

	log.Printf("server started on port %v", port)
	if err := http.ListenAndServe(port, mux); err != nil {
		log.Fatalf("unable to start server: %v", err)
	}
}
