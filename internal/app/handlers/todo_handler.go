package handlers

import (
	"encoding/json"
	"gitlab.com/abdirahman04/todo/internal/app/models"
	"gitlab.com/abdirahman04/todo/internal/app/services"
	"net/http"
	"strconv"
)

func Get(w http.ResponseWriter, r *http.Request) {
	todos, err := services.GetAll()
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("error fetching todos"))
		return
	}

	jsonTodos, err := json.Marshal(todos)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonTodos)
}
func GetById(w http.ResponseWriter, r *http.Request) {
}
func Post(w http.ResponseWriter, r *http.Request) {
	var todo models.TodoReq

	err := json.NewDecoder(r.Body).Decode(&todo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("unable to decode body"))
		return
	}

	err = services.Save(todo)
	if err != nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("unable to save todo"))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("todo saved successfully"))
}
func Put(w http.ResponseWriter, r *http.Request)        {}
func Delete(w http.ResponseWriter, r *http.Request)     {}
func DeleteById(w http.ResponseWriter, r *http.Request) {}
