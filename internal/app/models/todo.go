package models

type Todo struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Completed   bool   `json:"completed"`
}

type TodoReq struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

func NewTodo(req TodoReq, id int) Todo {
	return Todo{
		Id:          id,
		Title:       req.Title,
		Description: req.Description,
		Completed:   false,
	}
}
