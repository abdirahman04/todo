package services

import (
	"gitlab.com/abdirahman04/todo/internal/app/models"
	"gitlab.com/abdirahman04/todo/internal/app/repository"
)

func Save(req models.TodoReq) error {
	id := len(repository.TodoStore) + 1

	todo := models.NewTodo(req, id)

	err := repository.Add(todo)
	if err != nil {
		return err
	}

	return nil
}

func GetById(id int) (models.Todo, error) {
	todo, err := repository.GetById(id)
	if err != nil {
		return models.Todo{}, err
	}

	return todo, nil
}

func GetAll() ([]models.Todo, error) {
	todos, err := repository.GetAll()
	if err != nil {
		return nil, err
	}

	return todos, nil
}

func Update(todo models.Todo) error {
	if err := repository.Update(todo); err != nil {
		return err
	}

	return nil
}

func DeleteOne(id int) error {
	if err := repository.DeleteOne(id); err != nil {
		return err
	}

	return nil
}

func DeleteAll() error {
	if err := repository.DeleteAll(); err != nil {
		return err
	}

	return nil
}
