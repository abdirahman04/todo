package repository

import (
	"errors"
	"gitlab.com/abdirahman04/todo/internal/app/models"
)

var TodoStore []models.Todo

func Add(todo models.Todo) error {
	TodoStore = append(TodoStore, todo)

	return nil
}

func GetAll() ([]models.Todo, error) {
	return TodoStore, nil
}

func GetById(id int) (models.Todo, error) {
	for _, e := range TodoStore {
		if e.Id == id {
			return e, nil
		}
	}

	err := errors.New("no todo with given id found")
	return models.Todo{}, err
}

func Update(todo models.Todo) error {
	for _, e := range TodoStore {
		if e.Id == todo.Id {
			e.Title = todo.Title
			e.Description = todo.Description
			e.Completed = todo.Completed

			return nil
		}
	}

	return errors.New("no todo with given id found")
}

func DeleteOne(id int) error {
	for k, e := range TodoStore {
		if e.Id == id {
			TodoStore = append(TodoStore[:k], TodoStore[k+1:]...)
			return nil
		}
	}

	return errors.New("error deleting todo")
}

func DeleteAll() error {
	TodoStore = TodoStore[:0]

	return nil
}
